---
layout: default
title: IdeationHub
description: A social experience showcase that demonstrates design and development of Federated Ideation.
has_children: true
nav_order: 2
permalink: experiences/ideationhub
---

# IdeationHub
{: .fs-9 .no_toc }

Showcasing the design and development of an Ideation Hub on the Fediverse.
{: .fs-6 .fw-500 }

[Visit Project Page](https://coding.social/ecosystem/projects/ideationhub/){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Join Forum Discussion](https://discuss.coding.social/c/ideation-hub/10){: .btn .fs-5 .mb-4 .mb-md-0 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> IdeationHub is a **showcase** that is part of [Social Coding Movement](https://coding.social/ecosystem/projects/ideationhub/), and will become a full-blown and independent Free Software project. Ideation represents the first step of the [FSDL](hhttps://discuss.coding.social/t/brainstorm-components-of-the-free-software-development-lifecycle-wiki-post/53).

{: .todo}
> Refine frequently to clearly demonstrate the Fediverse SX process from start to finish.

## Showcase introduction

The sections below will lead through the design process of a new social experience for the Fediverse: an app for exchanging and brainstorming on ideas that wants to encourage people to organize projects that make them real.

We use four fictional stakeholders, [_Bob & Carol & Ted & Alice_](https://en.wikipedia.org/wiki/Bob_%26_Carol_%26_Ted_%26_Alice), who are all fedizens:

| Person | Stakeholder role |
| :--- | :--- |
| **Bob** _(he/him)_ | **Client group**. _Bob likes hanging out on the Fediverse, hosts his own Mastodon instance. But he would really like to be hosting a new type of application._ |
| **Carol** _(they/them)_ | **Client group**. _Carol seeks the discussion on the Fediverse, and is often involved in brainstorming how to make the world a better place._ |
| **Ted** _(they/them)_ | **Creator group**. _Ted is a UX **Designer**, follows artists on the Fediverse and exchanges UI design concepts._ |
| **Alice** _(she/her)_ | **Creator group**. _Alice is a die-hard and principled FOSS **Developer**. A passion for tech, she is matter-of-fact and likes to get things done._ |

The idea for building an IdeationHub arose during a Fediverse discussion, triggered by Carol:

{: .note}
> ***Carol***: _" Isn't it frustrating how we have some many great ideas here, only to see them lost in timeline history? I want a neat way for us to keep track of them. "_<br>
> ***Bob***: _" I fully agree, this frustrates me as well. We might make a federated app to collect and foster them, and find collaborators to turn them into projects. "_<br>
> ***Ted***: _" I can help turn this into a design that can be easily implemented. Let's start simple and then expand with more features. **@alice** are you interested to help? "_<br>
> ***Alice***: _" Yep, count me in, folks! Let's call this project **IdeationHub**. "_<br>
> ***Ted***: _" Fabulous. Let's get going. We can use Floorplanner to guide us through our [solution design](/floorplanner/design). "_<br>
> ***Carol***: _" Floorplanner looks nice. Feels like a recipe book. I found some good entry points with detailed instructions to help us [discover our needs](/process/recipes). "_

And so they started their Social Experience Design adventure.

## Project inception



