---
layout: default
title: Diagramming
parent: Domain Design
nav_order: 4
permalink: floorplanner/project/domain/design
---

# Diagramming domain

## Description

Floorplanner's diagrammer component is tailored for drawing specific diagram types as needed for social experience blueprints. The UI makes creation of these diagrams as easy as possible by restricting interaction based on the chosen diagram type.

Diagram types are provided from other domains to configure the diagrammer. They define the shape and connector elements that are allowed in the diagram, and the interactions available on selected elements. Furthermore they provide a set of validation rules for the diagram. This is required because an invalid diagram cannot be used for e.g. scaffolding code.

## Constraints

- Not a free-hand drawing tool but tailored for specific diagram types.
- Collaborative editing is a requirement, but not in scope for the MLP release.

## Architecture notes

Diagramming is a generic domain. Most of its features will be offered by existing libraries. For the "rough sketch" prototype an interface to [Miro](https://miro.com) SaaS was used. Though unsupported, it should continue to function. Diagramming is mostly front-end functionality, that drives domain logic via a Ports & Adapters interface. The bounded context of the domain has following main requirements:

- Provide means to extend with additional diagram types with relative ease.
- Provide a consistent diagram model to be presented to other domains.
- Provide sufficient domain events for representation of diagram interaction.

Diagramming only concerns visual appearance of the diagram, and representation in a consistent internal model. Elements on the diagram can have elaborate properties and trigger logic, yet all that takes place in other domains and is triggered by domain events. The diagram may contain decorative elements, such as explanatory text-boxes, that do not play a role in diagram validity.

A diagram can be persisted but the persistence isn't part of the domain either. Instead the diagrammer allows import and export of diagram definitions from other domains.