---
layout: default
title: Domain Design
parent: Floorplanner project
has_children: true
nav_order: 1
permalink: floorplanner/project/domain-design
---

# Domain Design
{: .no_toc }

## Social Experience Design

{: .note}
> **Scaffold an event-driven Elixir project based on the Domain Design of a social experience.**

The Minimum Lovable Process (MLP) of Floorplanner:

- Facilitates Domain Driven Design (DDD) by Creators and Clients.
- Where Domain Modeling informs the event-driven Architecture Design.
- From which an Elixir project is bootstrapped and the codebase scaffolded.

The top-level domain is called **Social Experience Design**.

![Bounded Context diagram of Social Experience Design top-level domain](/assets/images/design/social-experience-design_bounded-context.drawio.svg)

After decomposing, these are the bounded contexts for the constituent domains:

| Bounded Context | Description | Type |
| :--- | :--- |
| **Domain Modeling** | Model a social experience using domain driven design. | Core domain |
| **Architecture Design** | Align architecture design to domain models. | Supporting domain |
| **Scaffolding** | Keep project artifacts in sync with the social experience blueprint. | Supporting domain |
| **Diagramming** | Draw diagrams in support of domain models and architecture design. | Generic domain |
| **Project Management** | Manage state and lifecycle of the social experience blueprint. | Generic domain |

## Domain Modeling
<div markdown="1">
Core Domain
{: .label .label-purple }
</div>

#### Description

Any software has a domain model. Solidground provides Strategic Domain Design in Floorplanner, that allows Clients to follow an explicit process of elaborating their social experience. This approach ensures focus on their needs and allows non-technical stakeholders to stay informed as the software matures.

Domain Design is the structural decomposition of the social experience. Clients are primary actors, guided by Creators. Floorplanner UI presents the design as documentation artifact that is elaborated and enriched with diagram. It is persisted using the Project Management domain where it becomes part of the Solution Blueprint.

The Domain Design decomposes into one or more Domains, until each Domain expresses a single Bounded Context that can be modeled consistently into domain Concepts and their Relationships. Terminology of the bounded context is collected in the Ubiquitous Language. This terminology is also used for domain storytelling as a textual explanation of the domain.

#### Concept map

#### Domain storytelling

- **Domain Design** documents **Domains**.
- **Domain Design** defines **Domain Relationships**.
- **Domain Design** is modeled by a **Context Map**.
- **Bounded Context** determines **Ubiquitous Language**.
- **Bounded Context** defines a **Domain**.
- **Bounded Context** defines **Domain Concepts**.
- **Bounded Context** defines **Concept Relationships**.
- **Bounded Context** is modeled by a **Concept Map**.
- **Bounded Context** is documented by a **Domain Story**.
- **Domain Analyst** decomposes **Domains**.
- **Domain Analyst** diagrams **Context Map**.
- **Domain Analyst** diagrams **Bounded Context**.
- **Domain Analyst** determines **Domain Story**.

## Architecture Design
<div markdown="1">
Supporting Domain
{: .label .label-yellow }
</div>

#### Description

Architecture Design occurs in parallel with Domain Modeling where bounded contexts are informative to naming concepts of the event-driven architecture.

#### Constraints

- Architecture Design is restricted to opinionated EDA patterns.
- Domain Design bounded contexts are only informative.

#### Domain storytelling

- Architecture Design documents a Service Module.
- Service Module has an Architecture Template.
- Service Module is modeled by an 
- Actor
- Command
- Aggregate
- Process Manager
- Domain Event
- External System
- View Projection
- Field

## Diagramming
<div markdown="1">
Generic Domain
{: .label .label-grey }
</div>

#### Summary

- Diagrams support domain modeling, architecture design and scaffolding.
- Diagramming involves modeling based on pre-defined Diagram Types.
- Designer is the primary actor belonging to the Creators stakeholder group.
- Secondary actor (not listed) is the Domain Expert in the Client stakeholder group.

#### Concept map

#### Domain storytelling

- **Diagram Type** provides **Shape Elements**.
- **Diagram Type** provides **Connector Elements**.
- **Diagram Type** provides **Element Interactions**.
- **Diagram Type** provides **Validation Rules**.
- **Diagram Type** initializes the **Diagrammer**.
- **Diagrammer** imports a **Diagram**.
- **Diagrammer** exports a **Diagram**.
- **Diagrammer** provides **Canvas**.
- **Diagrammer** provides **Shape Palette**.
- **Diagrammer** provides **Connector Palette**.
- **Diagrammer** provides **Tool Palette**.
- **Tool Palette** contains **Element Selector**.
- **Tool Palette** contains **Text Editor**.
- **Tool Palette** contains **Frame Editor**.
- **Diagram** is displayed on the **Canvas**.
- **Diagram** is based on a **Diagram Type**.
- **Designer** selects a **Shape Element**.
- **Designer** draws a **Shape** on the Canvas.
- **Designer** interacts with a **Shape**.
- **Designer** selects a **Connector Element**.
- **Designer** draws a **Connector** on the Canvas.
- **Designer** interacts with a **Connector**.