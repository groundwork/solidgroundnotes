---
layout: default
title: Taskweave project
has_children: true
nav_order: 3
permalink: taskweave/project
---

# Taskweave project
{: .fs-9 .no_toc }

Project documentation for Taskweave crowdsourced process improvement.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---