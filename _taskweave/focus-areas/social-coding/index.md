---
layout: default
title: Social Coding
parent: Key focus areas
has_children: true
nav_order: 1
permalink: process/focus/social-coding
---

# Social Coding
{: .no_toc }

<br>

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> Solidground is practitioner of [Social Coding](https://coding.social) and also active member of the movement's [co-shared community](https://discuss.coding.social). We contribute our insights **upstream** and collaborate closely with other members.

## What is Social Coding?

## Towards the Peopleverse

## Substrate Formation

## Joyful Creation