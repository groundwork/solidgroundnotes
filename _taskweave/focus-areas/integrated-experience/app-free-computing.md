---
layout: default
title: App-less Computing
parent: Integrated Experience
nav_order: 3
permalink: process/focus/integrated-experience/appfree-computing
---

# App-free Computing
{: .no_toc }

<br>

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Introduction