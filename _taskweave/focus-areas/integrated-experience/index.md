---
layout: default
title: Integrated Experience
parent: Key focus areas
has_children: true
nav_order: 4
permalink: process/focus/integrated-experience
---

# Integrated Experience
{: .no_toc }

<br>

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Introducing App-less Computing

## Focus on interoperability

## Common ActivityPub extensions

{: .todo}
> Expand this section, as it is an important topic early on for project direction.

### Provide guidance on documentation best-practices

- A list of common namespace prefixes (e.g. `xsd` for XML Schema, `schema` for schema.org)

### How to define the JSON-LD @context

- The JSON-LD `@context` is defined in a separate `.jsonld` document.
- The `@context` is de-referencable and served from a stable URL.
- De-referencing with `application/json-ld` returns the JSON of the `@context`.
- De-referencing with `text/html` returns a nicely formatted reference page.

