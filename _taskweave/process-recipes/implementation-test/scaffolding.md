---
layout: default
title: Scaffolding
parent: Implementation & Test
nav_order: 1
permalink: process/implementation-test/scaffolding
---

# Scaffolding
{: .fs-9 .no_toc }

Bootstrap your codebase to reflect your domain design, architecture and configs and keep them in sync.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary

| | Generate code from a Floorplanner Blueprint. Floorplanner domain model diagrams, naming conventions, descriptions and metadata are used to tailor the code. |
| :--- | :--- |
| **When to use** | - To generate a brand new social experience project.<br>- To generate code files for an additional domain.<br>- To generate code files for selected parts of a domain diagram. |
| **Participants** | Developer (Creator group) |
| **Requirements** | - Access to Floorplanner social experience blueprint<br>- Consistent and valid [Event Storming](#event-storming) diagrams.
| **Duration** | Minutes. |
| **Expected outcome** | Depending on scope of the input:<br>- A new project with placeholder files.<br>- An updated project with additional placeholder files. |
| | [Get Started](domain-modeling/event-storming){: .btn .btn-primary .fs-5 .mb-4 .mb-md-0 .mr-2 } [Learn More](https://discuss.coding.social/c/solidground/13){: .btn .fs-5 .mb-4 .mb-md-0 } |