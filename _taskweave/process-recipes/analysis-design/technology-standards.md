---
layout: default
title: Technology Standards
parent: Analysis & Design
nav_order: 2
permalink: process/analysis-design/technology-standards
---

# Technology Standards
{: .fs-9 .no_toc }

TODO
{: .fs-6 .fw-500 }

{: .note}
> Adjust this pattern to your preferences.

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary

TODO

| | Track the status and outcome of decisions that affect the architecture design.  |
| :--- | :--- |
| **When to use** | - For any architecture decision with major impact on the implementation.<br>- To support non-functional requirements of the software.<br>- To ensure consensus about architecture direction in the project team. |
| **Participants** | - Person(s) investigating the decision.<br>- People affected by the decision (stakeholders). |
| **Requirements** | - Decisions are preceded by a [Spike](/process/recipes#spikes) to gather background information.<br>- Inform stakeholders, process their feedback, involve them in decision-making. |
| **Duration** | About one hour to document the decision. |
| **Expected outcome** | An architecture decision has been made. |

[➥ I need more info](/process/analysis-design/#technology-standards){: .btn .fs-5 .mb-4 .mb-md-0 }