---
layout: default
title: Shared Vision
parent: Inception Phase
nav_order: 2
permalink: process/inception-phase/shared-vision
---

# Shared Vision
{: .fs-9 .no_toc }

Gain a common understanding of the problems to address, and envision a high-level solution.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary