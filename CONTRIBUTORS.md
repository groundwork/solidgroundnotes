# Contributors

Many thanks to the following people for contributing to this website:

<!-- Add contributors alphabetically. -->

- Arnold Schrijver, @circlebuilder ([@humanetech](https://mastodon.social/@humanetech) on Fediverse)
