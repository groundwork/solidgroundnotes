---
layout: default
title: Glossary
nav_order: 3
permalink: glossary
---

## Glossary
{: .fs-9 .no_toc}

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> This Glossary represents the Solidground project's [_ubiquitous language_](/process/focus/domain-driven-design#ubiquitous-language) and is very important as a reference sheet to  our own domain design, product development and software implementation.

### About this page
{: .no_toc}

Here you find common language that we use throughout our initiative, and MUST be kept up-to-date. If you find important concepts that are missing, then please propose them in our channels or file an issue to our [documentation tracker](https://codeberg.org/solidground/solidgroundnotes/issues).

### Solidground Product Domain

This terminology is most likely to be transferred to UI's and the code itself, to name modules and variables.

| Term | Explanation |
| :---: | :--- |
| Blueprint | Also called Solution Blueprint. The design blueprint of a social experience that is being created with Floorplanner. It consists of all the living documentation and specification artifacts including the evolving codebase and configuration settings. |
| Blueprint Package | A Floorplanner Blueprint that is packaged in a single compressed file for download or transfer to other locations. The package contains the entire state of the Blueprint, and includes the code. |
| Client | Stakeholder group. Persons that needs the social experience, and wants to see their features and requirements implemented. They are 'domain experts' and 'customer'. |
| Creator | Stakeholder group. Any person involved in creating and evolving social experiences for the Fediverse. This group has people in many roles, like Developer, Designer, Tester, etc. |
| Project Discipline | A related set of activities, procedures and tasks that occur on an ongoing basis throughout the project lifecycle. |
| Project Phase | A discrete phase of the project lifecycle. |
| Process Playbook | An opinionated sequence of Process Recipes and Process Steps to achieve a specific goal. Examples are a playbook for creating a 'Website' or 'Project Community'. |
| Process Recipe | A detailed procedure or guideline that outlines the steps to perform tasks relating to Social Experience Design, or project and process improvements. |
| Process Step | A single activity or procedure that is part of a Process Recipe. |
| Service Module | A packaged part of the social experience that can be deployed on Groundwork. Usually it contains the functionality that relates to a discreet bounded context of the domain design. |
| Social Experience | The solution that is created and to be hosted on Groundwork. Other projects call this the "App" but we avoid the term, as it is too restrictive. Social experiences can deeply integrated with other experiences and we want avoid thinking in app silo's. |
| SX | Social Experience Design. Top-level domain of the Solidground project. This is what we offer and forms the basis for Product development. For us SX is all about translating real-world social activity to software-supported automated services for the Fediverse, and applying Social Coding principles. |

### Taskweave

This terminology is more explanatory of various concepts that are applied throughout the project, not necessarily returning in the implementation.

| Term | Explanation |
| :---: | :--- |
| Kaizen | In simple terms this means a focus on 'continual improvement'. But this Japanese concept goes further that just that, and encompasses a complete mindset and philosophy to 'change for better'. Never let an opportunity pass to introduce even the slightest improvement. |
| Process Guidance | The entirety of methods, mechanisms, both manual and automated, that are used to help Creators and Clients through the process of design and delivery of their social experiences. This can be documentation, checklists, wizards, automation scripts, etc. |

### Domain Driven Design

| Term | Explanation |
| :---: | :--- |
| Bounded Context | |
| DDD | Domain Driven Design. A structural approach to elaborate a field of knowledge or activity into requirements and ultimately the source code of a software application. |
| Read Model | A data model that is optimal to be queried by a user interface. Read models are updated by domain events, and often represent denormalized views of a domain model that avoid database overhead. |
| Ubiquitous Language | The terminology that all stakeholders agree to use consistently when referring to concepts of a particular domain. |

### Software Development

| Term | Explanation |
| :---: | :--- |
| EDA | Event Driven Architecture. A type of software architecture where event messages are exchanged between the various components of the architecture, usually over an event bus. |
| FOSS | Free and Open Source Software. This is equivalent to saying 'Free Software'. Free as in "Freedom", not "Gratis" and indicated by its license. We recognize approved licenses of Open Source Initiative (OSI) and Free Software Foundation (FSF). |
| FSDL | Free Software Development Lifecycle. Comprising all activities and processes that are relevant to software implementation from the project's inception to its end-of-life. |
| MLP | Minimum Lovable Process. Project stage where Taskweave and Floorplanner offer support for a small chunk of the FSDL, representable to demonstrate product philosophy. |
| MVP | Minimum Viable Platform. Project stage where Solidground  is able to host a Service Module and communicate with another instance of the platform via the Fediverse. |
| Scaffolding | Generate project structure and code from information gathered in a Floorplanner Blueprint design. |
| Social Coding | An inclusive approach to Free Software development that focuses on people's needs and fostering fostering of healthy projects, communities and ecosystems.
| Stakeholder | Any person or group that directly or indirectly benefits from the application or project and has needs and interests that must be taken into account. |

### Fediverse

| Term | Explanation |
| :---: | :--- |
| FEP | Fediverse Enhancement Process. A process defined by the SocialHub community to specify extensions to the ActivityPub protocol. |
