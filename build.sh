#!/usr/bin/env bash
rm -rf pages.git _site

bundle install
bundle exec jekyll build

REPO=${1:-git@codeberg.org:solidground/solidgroundnotes}
DATE=$(date -u -Is)
git clone $REPO pages.git
rsync -av _site/* pages.git/

cd pages.git &&
git checkout --orphan current &&
git add -A &&
git commit -m "Deployment at $DATE" &&
git branch -m pages &&
git push -f origin pages &&
cd .. &&
rm -rf pages.git
