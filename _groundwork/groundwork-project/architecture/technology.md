---
layout: default
title: Technology choices
parent: Architecture
nav_order: 2
permalink: groundwork/project/architecture/technology
---

# Groundwork Technology Choices
{: .no_toc }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Introduction

{: .todo}
> Turn explanation into a process recipe.

{: .hint}
> Detailed information about [Using Technology Radars](#) can be found in our process recipe.

This page keeps track of major decisions that relate to Groundwork technology stack. Here we explore libraries and frameworks that are candidates to adopt in support of various architecture concepts and requirements. Decisions are ordered in reverse chronological order (Newest-first) and have states. They represent a [Technology Radar](https://www.thoughtworks.com/radar/faq-and-more). It has one additional 'Open' state, meaning that an investigation, called a Spike, is planned.:

<div markdown="1">
Adopt
{: .label .label-green }

Trial
{: .label .label-blue }

Assess
{: .label .label-yellow }

Hold
{: .label .label-red }

Open
{: .label .label-grey }
</div>

## Decision Log

### Adopt Elixir PETAL technology stack

Adopt
{: .label .label-green }

The [PETAL](https://changelog.com/posts/petal-the-end-to-end-web-stack) stack is a popular combination of frameworks and libraries that have proven to be very powerful and flexible. It consists of:

- [**P**hoenix](https://www.phoenixframework.org/): Leading web framework for Elixir.
- [**E**lixir](https://elixir-lang.org/getting-started/introduction.html): Elixir server-side rendering code.
- [**T**ailwind](https://tailwindcss.com/): CSS framework to mark up HTML with semantic classes.
- [**A**lpine](https://alpinejs.dev/): Minimal framework for composing JS behaviours in markup.
- [**L**iveview](https://github.com/phoenixframework/phoenix_live_view): Real-time UX with server-rendered HTML.

Elixir and Phoenix Liveview were already part of the stack. Tailwind was a great experience in migration from Bootstrap for [fedi.foundation](https://fedi.foundation) and is intuitive to use for non-CSS experts.

### Use Elixir for Floorplanner and Groundwork Platform

Adopt
{: .label .label-green }

{: .todo}
> List more arguments that led to the decision to adopt.

Initially Golang was considered, based on pre-selection of Go-Fed ActivityPub library. But with both Bonfire and CPub and language characteristics in mind, Elixir became the preferred language environment. Listing investigation results of both environments.

### Use Golang for Floorplanner and Groundwork Platform

Hold
{: .label .label-red }

<details><summary>Expand for details on the Golang technology stack selection.</summary>
<p markdown="block">

Selection of technology stack for Golang was based on [Go-Fed](#candidate-go-fed) for the ActivityPub framework.

<img src="{{ 'assets/images/gofed-architecture-concept-optimized.svg' | absolute_url }}" alt="Go-Fed conceptual overview"/>



</p>
</details>

## ActivityPub support libraries

Starting point has been to build off existing [ActivityPub implementations](https://delightful.club/delightful-activitypub-development/), where we found:

| Name | Description | Language | License |
| :--- | :--- | :--- | :--- |
| [Go-Fed](https://github.com/go-fed/activity) | Complete ActivityStreams-based ontologies plus middleware handlers implementing ActivityPub. | Golang | BSD-3-Clause |
| [Bonfire](https://github.com/bonfire-networks) | Federated network for individuals and communities to design, operate and control their own digital lives. | Elixir | AGPL-3.0, MPL |
| [API Platform](https://github.com/api-platform) | Create REST and GraphQL APIs, scaffold Jamstack webapps, stream changes in real-time. | PHP, Typescript | MIT |
| [SemApps, ActivityPods](https://github.com/assemblee-virtuelle) | A toolbox to create semantic web applications. ActivityPub + Solid PODS = ❤️ | Javascript | Apache-2.0 |
| [openEngiadina CPub](https://codeberg.org/openEngiadina/cpub) | An experimental generic ActivityPub server that uses Semantic Web ideas. | Elixir | AGPL-3.0 |
| [rdf-pub](https://gitlab.com/linkedopenactors/rdf-pub/) | ActivityPub implementation, that is not limited to the activity-stream vocabulary, but supports RDF. | Java | EUPL |

Technology stacks based on Golang and Elixir have been explored in more detail in paragraphs below. The following sub-sections summarize considerations for each candidate.

### Candidate: Go-Fed
Not selected
{: .label .label-yellow }

Set of ActivityPub libraries implemented in Golang that focus on shielding the developer from complexity and focus on building ActivityPub vocabulary extensions.

Key features:
- Generate Golang code from OWL vocabulary definitions.
- Fills in the blanks for ActivityPub support, offering Webfinger, NodeInfo, HTTP signatures.

Cons:
- Libraries no longer actively maintained. Implementers create forks to extend.
- Extensibility mechanism still largely untested, not mature. Lack of documentation.
- Huge amount of code generated. Cannot be easily shared between service modules.

Advice: Collaborate with implementers on AP extensibility mechanisms, most notably with [forg.es](https://forg.es) community.

### Candidate: Bonfire
Considering
{: .label }

Very interesting extensible framework with intent to facilitate an open ecosystem of plugin extensions for the creation of social applications that have federation support.

Key features:
- Built as a Fediverse platform with out-of-the-box common social features (Microblogging).
- Well productized, vision of becoming an open collaboration platform.
- Extensibile plugin architecture and plans faciliate a plugin ecosystem.

Cons:
- Early stage of development, unclear short-term direction (microblogging-first approach?).
- Architectural complexity, opinionated approach, unclear positioning for plugin builders.

Advice: Strive for close collaboration, invited to join Social Coding co-shared community.

### Candidate: API Platform
Not selected
{: .label .label-yellow }

Very active project, offering a complete platform for productive development of front-end applications based on linked data and semantice web technologies.

Key features:
- Generate a PHP data model from the RDF vocabularies and ontologies.
- Production-ready application framework with existing ecosystem.

Cons:
- PHP language, opinionated Jamstack less suited for server foundation and polyglot usage.
- Full reliance on framework direction. ActivityPub is just one ecosystem component.

Advice: Interesting to learn semantic web best-practices. Possible partner for future collaboration.

### Candidate: SemApps, ActivityPods
Not selected
{: .label .label-yellow }

SemApps, a microservice platform based on Moleculer that adds extensive linked data and native ActivityPub support. ActivityPods is a SemApps application that combines ActivityPub and Solid.

Key features:
- Ease of use of Linked Data Platform to create, store and retrieve custom entities.
- Extensibility, modular architecture. Huge developer base on the NodeJS techstack.

Cons:
- More focused on Semantic Web, knowledge graphs, using experimental, complex standards.
- Strongly tied to underlying frameworks and NodeJS + Javascript technology ecosystem.

Advice: Interesting to learn semantic web best-practices. Possible partner for future collaboration.

### Candidate: openEngiadina CPub
Preferred
{: .label .label-green }

Generic ActivityPub server with experimental features based on research into peer-to-peer use. Provides support for Client-2-Server ActivityPub and content-based addressing.

Key features:
- Federation based on linked data (RDF) extensibility and a C2S generic server.

Cons:
- Experimental features that will not be ready for production on the short term.
- Project is discontinued due to AS/AP + C2S complexity. Continues based on XMPP.

### Candidate: rdf-pub
Not selected
{: .label .label-yellow }

ActivityPub server that supports Client-2-Server features and RDF based vocabularies. Part of Linked Open Actors project.

Key features:
- Linked data (RDF) and basic C2S support.

Cons:
- Very early development, small team, and serves a larger project with different use cases.