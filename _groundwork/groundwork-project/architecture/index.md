---
layout: default
title: Architecture
parent: Groundwork project
has_children: true
nav_order: 2
permalink: groundwork/project/architecture
---

# Groundwork architecture
{: .fs-9 .no_toc }

Federated communication and integration platform
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Architecture decisions

Below you find the list of architecture decision records (ADR's) for the Groundwork platform..

{: .hint}
> ADR's are based on the [Groundwork ADR template](adr-template) ([raw markdown](https://codeberg.org/solidground/solidgroundnotes/raw/branch/main/_groundwork/groundwork-project/architecture/adr-template.md)) and stored in `/docs/adr` of the code repository. See also the process recipe [Architecture Decisions](/process/analysis-design/architecture-decisions).

| Decision no. | Decision summary | Status |
| :--- | :--- | :--- |
| ADR-001| [Clean architecture project structure](https://codeberg.org/solidground/groundwork/pulls/2) <svg viewBox="0 0 24 24" height="12" width="12" aria-labelledby="svg-external-link-title"><use xlink:href="#svg-external-link"></svg> | proposed |

## Technology radar

Here interesting and innovative technologies are tracked. Technologies with status of `Adopt` are considered for implementation.

{: .hint}
> Update the radar by following instructions of the [Technology Radar](/process/analysis-design/technology-radar) Process Recipe.

## Technology stack

The technology stack contains a list of libraries and frameworks that have been selected.

{: .hint}
> Update techstack choices by following instructions of the [Techstack](/process/implementation-test/techstack) Process Recipe.