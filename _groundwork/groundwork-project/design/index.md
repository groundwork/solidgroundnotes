---
layout: default
title: Design
parent: Groundwork project
has_children: true
nav_order: 1
permalink: groundwork/project/design
---

# Design
{: .no_toc }

## Stakeholders

Groundwork is primarily targeted to Developers to make it easier to develop federated applications, especially ones that target specialized business domains.

<table>
  <thead>
    <tr>
      <th width="40%">Stakeholder</th>
      <th width="60%">Key benefits</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><b>Developer</b>: Primary stakeholder. Anyone that wants to build modular domain-specific application services that can be federated and are compliant to Fediverse open-standards.</td>
      <td>
        <ul>
          <li>Focus primarily on the feature set that matter to the application domain.</li>
          <li>Service platform handles the complexity of providing federation support, production-ready server features.</li>
          <li>Develop service modules in the languages you are most comfortable with.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Provider</b>: Secondary stakeholder. Party that offers an application or add-on, packaged as service modules.</td>
      <td>
        <ul>
          <li>Modularize your application in ways that fit your business model.</li>
          <li>Host your application on a stable platform that eases service management.</li>
          <li>Integrate and extend your application with additional service modules developed by third-parties.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Sysadmin</b>: Person responsible for maintaining a Groundwork server and its service module deployments.</td>
      <td>
        <ul>
          <li>Install, configure, and operate via a set of intuitive maintenance procedures.</li>
          <li>Stay in control, monitor health metrics and usage data in your admin dashboard.</li>
          <li>Consult great documentation about new platform releases and available service modules. Upgrade with ease.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Host</b>: Indirect (not shown in domain model). Party that provides Groundwork as a service platform.</td>
      <td>
        <ul>
          <li>Offer a production-ready Groundwork instance to Providers as a full-blown application platform.</li>
          <li>Multi-tenancy. Provide subscription plans to Tenants (not part of the MVP).</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Fedizen</b>: Indirect (not shown in domain model). Person with accounts on the service platform.</td>
      <td>
        <ul>
          <li>Has full control to manage their presence on the platform.</li>
          <li>Uses the application features offered by the platform Provider.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<br/>


## Application domains

Apart from the layering presented above there are four functional areas, or application domains:

| Domain | Description |
| :--- | : :--- |
| **Groundwork Core** | Common server features, such as configuration, logging, identity and authentication, security, federation endpoints, and more. |
| **Groundwork Service Modules** | Framework for creation of dynamically loaded Service Modules. This includes bootstrap projects to jumpstart development. |
| **Groundwork Service Administration** | Facilities for Sysadmins and Providers to deploy and manage Service Modules at runtime. |
| **Groundwork Account Management** | Facilities for managing Member Accounts on the platform. |
