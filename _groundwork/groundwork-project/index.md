---
layout: default
title: Groundwork project
has_children: true
nav_order: 2
permalink: groundwork/project
---

# Groundwork project
{: .fs-9 .no_toc }

Project documentation for the Groundwork federated service platform.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Conceptual overview

Presenting meaningful information. Isn't that what all applications do? Sure, but what sets Groundwork apart is that it is a framework that helps you to make all of this easier. Let's examine the three conceptual layers of the service platform architecture:

- **Access layer**: Defines the _vocabulary_ of your _content_ as Linked Open Data so that it can be processed as _semantically meaningful information_ by your application and adheres to open communication standards and protocols that allow interaction with remote Groundwork instances and other federated applications.

- **Services layer**: Encapsulates the _functionality_ of your application as one or more _service modules_, and _exposes_ it via API's on top of which you can build user interfaces and which other service modules or remote applications can _integrate_ with. Service modules are _managed_ and _administered_ by the Groundwork service platform.

- **Application layer**: Provides utilities and libraries to ease application development. Rapid Application Development (RAD) tools that help create data entry screens, workflows, screen layouts and visual style. This layer adds conveniences and utilities are opinonated and optional.

<img src="{{ 'assets/images/groundwork-service-platform-concept-detailed_optimized.svg' | absolute_url }}" alt="Groundwork conceptual overview">

The goal of the functionality in these layers is to provide Developers with a _low code_ environment on top of which they build their own modular applications.

### Meaningful services

Service Module support is handled in the _meaningful services_ layer, where we find:

- **service**: defines and implements the concept of Service Modules.
- **management**: provides Service Management and deployment at runtime.

### Information access

The core API's handle server information processing:

- **content**: information storage and management in different storage providers.
- **federation**: communication and interaction with other decentralized systems.

### Present application

Helper libraries to ease user interaction with the application:

- **form**: ease formatted data input according to application domain.
- **style** decoupled styling of the application interface.
- **ux**: interaction patterns and application workflows.
- **ui**: reusable user interface components and widgets.

## Major requirements

### Service modules

Groundwork uses a dynamic plugin architecture to support Service modules. The appropriate implementation of this system is one of the core challenges of the project. Here are some of its requirements:

- **Dynamic**: Pluggable installation, activation and deactivation at run-time.
- **Polyglot**: Interoperable service modules are programming language-independent.
- **Federated**: Service module developers have full control of federation behavior.
- **Globalized**: Supports both internationalization and localization of applications.
- **Semantic**: Brings the power of Linked Data to developers, without its complexity.
- **Intuitive**: Should be easy to use, operate and upgrade by all stakeholders.

## Minimum viable platform

{: .important }
Application layer utilities are NOT part of the Backend-as-a-Service MVP of Groundwork.

{: .important }
Groundwork MVP is NOT multi-tenant. It runs a single application only.